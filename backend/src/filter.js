const { isEmpty } = require("lodash");
const _ = require("lodash");


exports.filter = (data, search) => {
  console.log(data, search);
  search = _.capitalize(search);

  // Search substring in the all result
  let result = _.map(data, (data) => {
    return _.filter(data.results, (data) => {
      let name = _.capitalize(data.name);
      return name.search(search) >= 0;
    });
  });

  // Delete empty result
  result = _.filter(result, (data) => {
    return !_.isEmpty(data)
  });
  console.log(search, result);
  return result;
};
