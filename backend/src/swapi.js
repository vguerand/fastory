
//    const schemaSwapi = axios
//    .get("https://swapi.dev/api/")
//    .then((reponse) => reponse.data)
//    .catch((erreur) => console.log(erreur));
const schemaSwapi = [
    "https://swapi.dev/api/starships/",
    "https://swapi.dev/api/films/",
    "https://swapi.dev/api/people/",
    "https://swapi.dev/api/vehicles/",
    "https://swapi.dev/api/starships/",
    "http://swapi.dev/api/planets/",
    "https://swapi.dev/api/starships/?page=2",
    "https://swapi.dev/api/people/?page=2",
    "https://swapi.dev/api/vehicles/?page=2",
    "https://swapi.dev/api/starships/?page=2",
    "http://swapi.dev/api/planets/?page=2",
    "https://swapi.dev/api/starships/?page=3",
    "https://swapi.dev/api/people/?page=3",
    "https://swapi.dev/api/vehicles/?page=3",
    "https://swapi.dev/api/starships/?page=3",
    "http://swapi.dev/api/planets/?page=3",
]
const axios = require("axios");



exports.getAllResult = async () => {
    const allResult  =  Promise.all(schemaSwapi.map( async (el) => {
        return axios
        .get(el)
        .then((reponse) => reponse.data)
        .catch((erreur) => console.log(erreur));
    }));
    console.log(allResult);
    return allResult;
}


