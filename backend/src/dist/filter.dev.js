"use strict";

var _require = require("lodash"),
    isEmpty = _require.isEmpty;

var _ = require("lodash");

exports.filter = function (data, search) {
  console.log(data, search);
  search = _.capitalize(search); // Search substring in the all result

  var result = _.map(data, function (data) {
    return _.filter(data.results, function (data) {
      var name = _.capitalize(data.name);

      return name.search(search) >= 0;
    });
  }); // Delete empty result


  result = _.filter(result, function (data) {
    return !_.isEmpty(data);
  });
  console.log(search, result);
  return result;
};