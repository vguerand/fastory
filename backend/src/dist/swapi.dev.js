"use strict";

//    const schemaSwapi = axios
//    .get("https://swapi.dev/api/")
//    .then((reponse) => reponse.data)
//    .catch((erreur) => console.log(erreur));
var schemaSwapi = ["https://swapi.dev/api/starships/", "https://swapi.dev/api/films/", "https://swapi.dev/api/people/", "https://swapi.dev/api/vehicles/", "https://swapi.dev/api/starships/", "http://swapi.dev/api/planets/", "https://swapi.dev/api/starships/?page=2", "https://swapi.dev/api/people/?page=2", "https://swapi.dev/api/vehicles/?page=2", "https://swapi.dev/api/starships/?page=2", "http://swapi.dev/api/planets/?page=2", "https://swapi.dev/api/starships/?page=3", "https://swapi.dev/api/people/?page=3", "https://swapi.dev/api/vehicles/?page=3", "https://swapi.dev/api/starships/?page=3", "http://swapi.dev/api/planets/?page=3"];

var axios = require("axios");

exports.getAllResult = function _callee2() {
  var allResult;
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          allResult = Promise.all(schemaSwapi.map(function _callee(el) {
            return regeneratorRuntime.async(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    return _context.abrupt("return", axios.get(el).then(function (reponse) {
                      return reponse.data;
                    })["catch"](function (erreur) {
                      return console.log(erreur);
                    }));

                  case 1:
                  case "end":
                    return _context.stop();
                }
              }
            });
          }));
          console.log(allResult);
          return _context2.abrupt("return", allResult);

        case 3:
        case "end":
          return _context2.stop();
      }
    }
  });
};