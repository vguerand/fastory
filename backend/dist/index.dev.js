"use strict";

var Hapi = require("@hapi/hapi");

var axios = require("axios");

var _require = require("./src/swapi"),
    getAllResult = _require.getAllResult;

var _require2 = require("./src/filter"),
    filter = _require2.filter;

var Bcrypt = require("bcrypt");

var fs = require("fs");

var readData = function readData() {
  return JSON.parse(fs.readFileSync("data.json"));
}; // To store Data in local storage


var storeData = function storeData(data) {
  try {
    fs.writeFileSync("data.json", JSON.stringify(data));
  } catch (err) {
    console.error(err);
  }
};

var download = function download() {
  getAllResult().then(function (data) {
    return storeData(data);
  });
}; // Default User: Luke DadSucks


var users = {
  Luke: {
    username: "Luke",
    password: "$2y$12$N646Q5T2sUFUmS4G.HpiXeCrh3owvm8z23ZRgRVlm2hgumWenqOZu",
    // 'DadSucks'
    name: "Luke Doe",
    id: "2133d32a"
  }
}; // Validate for Authentification

var validate = function validate(request, username, password) {
  var user, isValid, credentials;
  return regeneratorRuntime.async(function validate$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          user = users[username];

          if (user) {
            _context.next = 3;
            break;
          }

          return _context.abrupt("return", {
            credentials: null,
            isValid: false
          });

        case 3:
          _context.next = 5;
          return regeneratorRuntime.awrap(Bcrypt.compare(password, user.password));

        case 5:
          isValid = _context.sent;
          credentials = {
            id: user.id,
            name: user.name
          };
          return _context.abrupt("return", {
            isValid: isValid,
            credentials: credentials
          });

        case 8:
        case "end":
          return _context.stop();
      }
    }
  });
};

var init = function init() {
  var server;
  return regeneratorRuntime.async(function init$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          download();
          server = Hapi.server({
            port: 3030,
            host: "localhost",
            routes: {
              cors: true
            }
          }); // Test for optionnel authentification

          _context2.next = 4;
          return regeneratorRuntime.awrap(server.register(require("@hapi/basic")));

        case 4:
          server.auth.strategy("simple", "basic", {
            validate: validate
          });
          server.route({
            method: "GET",
            path: "/login",
            options: {
              auth: "simple"
            },
            handler: function handler(request, h) {
              return "login";
            }
          }); // TODO download all the results in local

          server.route({
            method: "GET",
            path: "/download",
            handler: function handler(request, h) {
              download();
              return "SUCCES";
            }
          }); // Endpoint
          //   http://localhost:3030/?search=luke

          server.route({
            method: "GET",
            path: "/",
            handler: function handler(request, h) {
              if (request.query["search"] != undefined) {
                var test = filter(readData(), request.query["search"]);
                console.log(test, "test", readData());
                return test;
              }

              return "<div>\n      <h4> You can use the search parameter</h4>\n      <code> Exemple: http://localhost:3030/?search=luke </code>\n      </div>";
            }
          }); // How to use
          //   http://localhost:3030/schema
          //
          // or if you want a specific schema
          //   http://localhost:3030/schema?name=films

          server.route({
            method: "GET",
            path: "/schema",
            handler: function handler(request, h) {
              if (request.query["name"] != undefined) {
                return axios.get("https://swapi.dev/api/" + request.query["name"] + "/schema").then(function (reponse) {
                  return reponse.data;
                })["catch"](function (erreur) {
                  return console.log(erreur);
                });
              }

              return axios.get("https://swapi.dev/api/?format=json").then(function (reponse) {
                return reponse.data;
              })["catch"](function (erreur) {
                return console.log(erreur);
              });
            }
          });
          server.route({
            method: "GET",
            path: "/people",
            handler: function handler(request, h) {
              if (request.query["page"] != undefined) {
                return axios.get("http://swapi.dev/api/people/?page=" + request.query["page"]).then(function (reponse) {
                  return reponse.data;
                })["catch"](function (erreur) {
                  return console.log(erreur);
                });
              }

              return axios.get("http://swapi.dev/api/people/").then(function (reponse) {
                return reponse.data;
              })["catch"](function (erreur) {
                return console.log(erreur);
              });
            }
          });
          server.route({
            method: "GET",
            path: "/planets",
            handler: function handler(request, h) {
              if (request.query["page"] != undefined) {
                return axios.get("http://swapi.dev/api/planets/?page=" + request.query["page"]).then(function (reponse) {
                  return reponse.data;
                })["catch"](function (erreur) {
                  return console.log(erreur);
                });
              }

              return axios.get("http://swapi.dev/api/planets/").then(function (reponse) {
                return reponse.data;
              })["catch"](function (erreur) {
                return console.log(erreur);
              });
            }
          });
          server.route({
            method: "GET",
            path: "/films",
            handler: function handler(request, h) {
              if (request.query["page"] != undefined) {
                return axios.get("http://swapi.dev/api/films/?page=" + request.query["page"]).then(function (reponse) {
                  return reponse.data;
                })["catch"](function (erreur) {
                  return console.log(erreur);
                });
              }

              return axios.get("http://swapi.dev/api/films/").then(function (reponse) {
                return reponse.data;
              })["catch"](function (erreur) {
                return console.log(erreur);
              });
            }
          });
          server.route({
            method: "GET",
            path: "/species",
            handler: function handler(request, h) {
              if (request.query["page"] != undefined) {
                return axios.get("http://swapi.dev/api/species/?page=" + request.query["page"]).then(function (reponse) {
                  return reponse.data;
                })["catch"](function (erreur) {
                  return console.log(erreur);
                });
              }

              return axios.get("http://swapi.dev/api/species/").then(function (reponse) {
                return reponse.data;
              })["catch"](function (erreur) {
                return console.log(erreur);
              });
            }
          });
          server.route({
            method: "GET",
            path: "/vehicles",
            handler: function handler(request, h) {
              if (request.query["page"] != undefined) {
                return axios.get("http://swapi.dev/api/vehicles/?page=" + request.query["page"]).then(function (reponse) {
                  return reponse.data;
                })["catch"](function (erreur) {
                  return console.log(erreur);
                });
              }

              return axios.get("http://swapi.dev/api/vehicles/").then(function (reponse) {
                return reponse.data;
              })["catch"](function (erreur) {
                return console.log(erreur);
              });
            }
          });
          server.route({
            method: "GET",
            path: "/starships",
            handler: function handler(request, h) {
              if (request.query["page"] != undefined) {
                return axios.get("http://swapi.dev/api/starships/?page=" + request.query["page"]).then(function (reponse) {
                  return reponse.data;
                })["catch"](function (erreur) {
                  return console.log(erreur);
                });
              }

              return axios.get("http://swapi.dev/api/starships/").then(function (reponse) {
                return reponse.data;
              })["catch"](function (erreur) {
                return console.log(erreur);
              });
            }
          });
          _context2.next = 17;
          return regeneratorRuntime.awrap(server.start());

        case 17:
          console.log("Server running on %s", server.info.uri);

        case 18:
        case "end":
          return _context2.stop();
      }
    }
  });
};

process.on("unhandledRejection", function (err) {
  console.log(err);
  process.exit(1);
});
init();