const Hapi = require("@hapi/hapi");
const axios = require("axios");
const { getAllResult } = require("./src/swapi");
const { filter } = require("./src/filter");

const Bcrypt = require("bcrypt");
const fs = require("fs");

const readData = () => {
  return JSON.parse(fs.readFileSync("data.json"));
};

// To store Data in local storage
const storeData = (data) => {
  try {
    fs.writeFileSync("data.json", JSON.stringify(data));
  } catch (err) {
    console.error(err);
  }
};


const download = () => {
  getAllResult().then(data => storeData(data));
}

// Default User: Luke DadSucks
const users = {
  Luke: {
    username: "Luke",
    password: "$2y$12$N646Q5T2sUFUmS4G.HpiXeCrh3owvm8z23ZRgRVlm2hgumWenqOZu", // 'DadSucks'
    name: "Luke Doe",
    id: "2133d32a",
  },
};

// Validate for Authentification
const validate = async (request, username, password) => {
  const user = users[username];
  if (!user) {
    return { credentials: null, isValid: false };
  }

  const isValid = await Bcrypt.compare(password, user.password);
  const credentials = { id: user.id, name: user.name };

  return { isValid, credentials };
};

const init = async () => {

  download();
  const server = Hapi.server({
    port: 3030,
    host: "localhost",
    routes: {
      cors: true,
    },
  });

  // Test for optionnel authentification
  await server.register(require("@hapi/basic"));
  server.auth.strategy("simple", "basic", { validate });

  server.route({
    method: "GET",
    path: "/login",
    options: {
      auth: "simple",
    },
    handler: (request, h) => {
      return "login";
    },
  });

  // TODO download all the results in local
  server.route({
    method: "GET",
    path: "/download",
    handler: (request, h) => {
      download();
      return "SUCCES";
    },
  });

  // Endpoint
  //   http://localhost:3030/?search=luke
  server.route({
    method: "GET",
    path: "/",
    handler: (request, h) => {
      if (request.query["search"] != undefined) {
        let test = filter(readData(), request.query["search"])
        console.log(test, "test", readData())
        return test;
      }
      return `<div>
      <h4> You can use the search parameter</h4>
      <code> Exemple: http://localhost:3030/?search=luke </code>
      </div>`;
    },
  });

  // How to use
  //   http://localhost:3030/schema
  //
  // or if you want a specific schema
  //   http://localhost:3030/schema?name=films
  server.route({
    method: "GET",
    path: "/schema",
    handler: (request, h) => {
      if (request.query["name"] != undefined) {
        return axios
          .get("https://swapi.dev/api/" + request.query["name"] + "/schema")
          .then((reponse) => reponse.data)
          .catch((erreur) => console.log(erreur));
      }
      return axios
        .get("https://swapi.dev/api/?format=json")
        .then((reponse) => reponse.data)
        .catch((erreur) => console.log(erreur));
    },
  });

  server.route({
    method: "GET",
    path: "/people",
    handler: (request, h) => {
      if (request.query["page"] != undefined) {
        return axios
          .get("http://swapi.dev/api/people/?page=" + request.query["page"])
          .then((reponse) => reponse.data)
          .catch((erreur) => console.log(erreur));
      }
      return axios
        .get("http://swapi.dev/api/people/")
        .then((reponse) => reponse.data)
        .catch((erreur) => console.log(erreur));
    },
  });

  server.route({
    method: "GET",
    path: "/planets",
    handler: (request, h) => {
      if (request.query["page"] != undefined) {
        return axios
          .get("http://swapi.dev/api/planets/?page=" + request.query["page"])
          .then((reponse) => reponse.data)
          .catch((erreur) => console.log(erreur));
      }
      return axios
        .get("http://swapi.dev/api/planets/")
        .then((reponse) => reponse.data)
        .catch((erreur) => console.log(erreur));
    },
  });

  server.route({
    method: "GET",
    path: "/films",
    handler: (request, h) => {
      if (request.query["page"] != undefined) {
        return axios
          .get("http://swapi.dev/api/films/?page=" + request.query["page"])
          .then((reponse) => reponse.data)
          .catch((erreur) => console.log(erreur));
      }
      return axios
        .get("http://swapi.dev/api/films/")
        .then((reponse) => reponse.data)
        .catch((erreur) => console.log(erreur));
    },
  });

  server.route({
    method: "GET",
    path: "/species",
    handler: (request, h) => {
      if (request.query["page"] != undefined) {
        return axios
          .get("http://swapi.dev/api/species/?page=" + request.query["page"])
          .then((reponse) => reponse.data)
          .catch((erreur) => console.log(erreur));
      }
      return axios
        .get("http://swapi.dev/api/species/")
        .then((reponse) => reponse.data)
        .catch((erreur) => console.log(erreur));
    },
  });

  server.route({
    method: "GET",
    path: "/vehicles",
    handler: (request, h) => {
      if (request.query["page"] != undefined) {
        return axios
          .get("http://swapi.dev/api/vehicles/?page=" + request.query["page"])
          .then((reponse) => reponse.data)
          .catch((erreur) => console.log(erreur));
      }
      return axios
        .get("http://swapi.dev/api/vehicles/")
        .then((reponse) => reponse.data)
        .catch((erreur) => console.log(erreur));
    },
  });
  server.route({
    method: "GET",
    path: "/starships",
    handler: (request, h) => {
      if (request.query["page"] != undefined) {
        return axios
          .get("http://swapi.dev/api/starships/?page=" + request.query["page"])
          .then((reponse) => reponse.data)
          .catch((erreur) => console.log(erreur));
      }
      return axios
        .get("http://swapi.dev/api/starships/")
        .then((reponse) => reponse.data)
        .catch((erreur) => console.log(erreur));
    },
  });

  await server.start();
  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
