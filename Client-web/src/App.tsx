import React, { ReactElement, useState } from "react";
import { connect } from "react-redux";
import Routes from "./Routes";
import Navbar from "./components/Navbar/Navbar";

import Connexion from "./services/connexion";
import Tour from 'reactour';
import TourConfig from "./components/Tour/TourConfig"
import { BrowserView, MobileView } from 'react-device-detect';



function App(): ReactElement {

  return (
    <div className="bg-light" style={{
      backgroundImage: "url(stars.jpg)",
    }}>
      <Connexion>
        <BrowserView>
          <Navbar />
          {/* <h1>This is rendered only in browser</h1> */}
          <Routes />
        </BrowserView>
        <MobileView>
          <Navbar />
          {/* <h1>This is rendered only on mobile</h1> */}
          <Routes />
        </MobileView>
      </Connexion>
    </div>
  );
}

export default connect((store) => store)(App);

