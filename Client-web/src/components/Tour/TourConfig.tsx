import React from "react"
import wookies from './wookies.png'; // Tell webpack this JS file uses this image

const tourConfig = [
    {
      selector: '[data-tut="reactour__title"]',
      content:
        "Ce projet est un test technique ",
    },
    {
      selector: '[data-tut="reactour__sujet"]',
      content: 'Vous avez accès au sujet ici',
      position: [0, 20],
    },
    // {
    //   selector: '[data-tut="reactour__search_filters"]',
    //   content: `Vous pouvez effectuer des recherches et rajouter des filtres ici`,
    // },
    {
      selector: '[data-tut="reactour__search_nav"]',
      content: `Vous pouvez effecture la recherche depuis la barre de navigation`
    },
    {
        selector: '[data-tut="reactour__list"]',
        content: `La liste de chaque catégorie est aussi disponible`
    },
    {
      selector: '',
      content: () => (
          <p>
          Un certain Luke a le droit de se connecter au
          <a href="http://localhost:3030/login"> serveur </a>
          </p>
      )
      },
      {
        selector: '',
        content: () => (
            <p>
              Bonne chance
              {/* <Image  */}
              <img src={wookies} alt="Logo" />
            </p>
        ),
      
    }
  ]

  export default tourConfig;