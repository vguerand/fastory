"use strict";
exports.__esModule = true;
var react_1 = require("react");
var wookies_png_1 = require("./wookies.png"); // Tell webpack this JS file uses this image
var tourConfig = [
    {
        selector: '[data-tut="reactour__title"]',
        content: "Ce projet est un test technique "
    },
    {
        selector: '[data-tut="reactour__sujet"]',
        content: 'Vous avez accès au sujet ici',
        position: [0, 20]
    },
    // {
    //   selector: '[data-tut="reactour__search_filters"]',
    //   content: `Vous pouvez effectuer des recherches et rajouter des filtres ici`,
    // },
    {
        selector: '[data-tut="reactour__search_nav"]',
        content: "Vous pouvez effecture la recherche depuis la barre de navigation"
    },
    {
        selector: '[data-tut="reactour__list"]',
        content: "La liste de chaque cat\u00E9gorie est aussi disponible"
    },
    {
        selector: '',
        content: function () { return (react_1["default"].createElement("p", null,
            "Un certain Luke a le droit de se connecter au",
            react_1["default"].createElement("a", { href: "http://localhost:3030/login" }, " serveur "))); }
    },
    {
        selector: '',
        content: function () { return (react_1["default"].createElement("p", null,
            "Bonne chance",
            react_1["default"].createElement("img", { src: wookies_png_1["default"], alt: "Logo" }))); }
    }
];
exports["default"] = tourConfig;
