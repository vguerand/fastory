"use strict";
exports.__esModule = true;
var react_1 = require("react");
var moment_1 = require("moment");
require("./Timeline.css");
var DetailedSheet_1 = require("../Sheet/DetailedSheet");
function Article(props) {
    return (react_1["default"].createElement("div", { key: props.key },
        react_1["default"].createElement("div", { className: "separator text-muted" },
            react_1["default"].createElement("time", null, props.date)),
        react_1["default"].createElement("article", { className: "panel panel-primary" },
            react_1["default"].createElement("div", { className: "panel-heading icon" },
                react_1["default"].createElement("i", { className: "glyphicon glyphicon-plus" })),
            react_1["default"].createElement("div", { className: "panel-heading" },
                react_1["default"].createElement("h2", { className: "panel-title" }, props.title)),
            react_1["default"].createElement("div", { className: "panel-body" }, props.description),
            react_1["default"].createElement(DetailedSheet_1.ObjectSheet, { info: props.info }))));
}
function Timeline(props) {
    return (react_1["default"].createElement("div", { className: "timeline bg-dark text-white" },
        react_1["default"].createElement("div", { className: "line text-muted" }),
        props.listTime ? props.listTime.map(function (el, i) {
            return react_1["default"].createElement(Article, { title: el.name || el.title, description: el.classification || el.producer, date: moment_1["default"](el.created).format('MMM DD YYYY h:mm A'), info: el, key: i });
        }) : ""));
}
exports["default"] = Timeline;
