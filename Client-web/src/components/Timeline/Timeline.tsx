import React from "react";
import moment from "moment"
import "./Timeline.css"
import { ObjectSheet } from "../Sheet/DetailedSheet";

interface TimeInterface {
    date: string;
    description: string;
    title: string;
    info?: any;
    key: number;
}


function Article(props: TimeInterface): React.ReactElement {
    return (
        <div key={props.key}>
              {/* Separator  */}
              <div className="separator text-muted">
                <time>{props.date}</time>
            </div>
            {/* /Separator  */}

            <article className="panel panel-primary">
                {/* Icon  */}
                <div className="panel-heading icon">
                    <i className="glyphicon glyphicon-plus"></i>
                </div>
                {/* /Icon  */}
                {/* Heading  */}
                <div className="panel-heading">
                    <h2 className="panel-title">{props.title}</h2>
                </div>
                {/* /Heading  */}

                {/* Body  */}
                <div className="panel-body">
                    {props.description}
                 </div>
                {/* /Body  */}
                <ObjectSheet info={props.info} />
            </article>
        </div>
    )
}

interface TimelineProps {
    listTime: any;
}

function Timeline(props: TimelineProps): React.ReactElement {
    return (
        <div className="timeline bg-dark text-white">
            <div className="line text-muted"></div>
            {props.listTime ? props.listTime.map((el, i) => {
                return <Article title={el.name || el.title} description={el.classification || el.producer} date={moment(el.created).format('MMM DD YYYY h:mm A')} info={el} key={i}/>
            }): ""}
        </div>
    )
}

export default Timeline;