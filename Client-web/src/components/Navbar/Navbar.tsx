import React, { ReactElement, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { Navbar, Nav, NavItem, NavDropdown, Form, FormControl, Button, Card } from 'react-bootstrap';


import { connect } from "react-redux";
// import User from "../../interfaces/User.interface";
// import Notify from "../Notify";

function NavbarComponent(): ReactElement {
  const [search, setSearch] = useState('');
  const history = useHistory();
  //   const logout = () => {
  //     Notify(
  //       "Déconnexion réussie",
  //       "Vous serez redirigé dans quelques instants",
  //       "success"
  //     );
  //     localStorage.removeItem("user");
  //     localStorage.removeItem("code");
  //     setTimeout(() => window.location.reload(), 1500);
  //   };
  return (
    <Navbar bg="dark" variant="dark"  >
      <Navbar.Brand href="/" data-tut="reactour__title">Star Wars API React App </Navbar.Brand>
      <Nav className="mr-auto" data-tut="reactour__list">
        <Nav.Link href="/vehicles">Vehicles</Nav.Link>
        <Nav.Link href="/films">Films</Nav.Link>
        <Nav.Link href="/species">Species</Nav.Link>
        <Nav.Link href="/planets">Planets</Nav.Link>
        <Nav.Link href="/starships">Starships</Nav.Link>
      </Nav>
      <Form inline data-tut="reactour__search_nav">
        <FormControl type="text" placeholder="Search" value={search} onChange={(e)=> setSearch(e.target.value)}  className="mr-sm-4" />
        <Nav.Link href={"/result?search=" + search} >Search</Nav.Link>


      </Form>
    </Navbar>
  );
}
export default connect((store) => store)(NavbarComponent);