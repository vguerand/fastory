"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_router_dom_1 = require("react-router-dom");
var react_bootstrap_1 = require("react-bootstrap");
var react_redux_1 = require("react-redux");
// import User from "../../interfaces/User.interface";
// import Notify from "../Notify";
function NavbarComponent() {
    var _a = react_1.useState(''), search = _a[0], setSearch = _a[1];
    var history = react_router_dom_1.useHistory();
    //   const logout = () => {
    //     Notify(
    //       "Déconnexion réussie",
    //       "Vous serez redirigé dans quelques instants",
    //       "success"
    //     );
    //     localStorage.removeItem("user");
    //     localStorage.removeItem("code");
    //     setTimeout(() => window.location.reload(), 1500);
    //   };
    return (react_1["default"].createElement(react_bootstrap_1.Navbar, { bg: "dark", variant: "dark" },
        react_1["default"].createElement(react_bootstrap_1.Navbar.Brand, { href: "/", "data-tut": "reactour__title" }, "Star Wars API React App "),
        react_1["default"].createElement(react_bootstrap_1.Nav, { className: "mr-auto", "data-tut": "reactour__list" },
            react_1["default"].createElement(react_bootstrap_1.Nav.Link, { href: "/vehicles" }, "Vehicles"),
            react_1["default"].createElement(react_bootstrap_1.Nav.Link, { href: "/films" }, "Films"),
            react_1["default"].createElement(react_bootstrap_1.Nav.Link, { href: "/species" }, "Species"),
            react_1["default"].createElement(react_bootstrap_1.Nav.Link, { href: "/planets" }, "Planets"),
            react_1["default"].createElement(react_bootstrap_1.Nav.Link, { href: "/starships" }, "Starships")),
        react_1["default"].createElement(react_bootstrap_1.Form, { inline: true, "data-tut": "reactour__search_nav" },
            react_1["default"].createElement(react_bootstrap_1.FormControl, { type: "text", placeholder: "Search", value: search, onChange: function (e) { return setSearch(e.target.value); }, className: "mr-sm-4" }),
            react_1["default"].createElement(react_bootstrap_1.Nav.Link, { href: "/result?search=" + search }, "Search"))));
}
exports["default"] = react_redux_1.connect(function (store) { return store; })(NavbarComponent);
