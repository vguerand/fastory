import React, { lazy, Component, Suspense, useState, Fragment } from 'react';
import { importMDX } from 'mdx.macro'
import Jumbotron from 'react-bootstrap/Jumbotron';


function Content() {
    const MDX = lazy(() => importMDX('./Content.mdx'));

    return (
        <Fragment>
        <Suspense fallback={< div > Loading...</div >} >
            <Jumbotron data-tut="reactour__sujet">
                <MDX />
            </Jumbotron>
        </Suspense>
        </Fragment>
        );
}

export default Content;
