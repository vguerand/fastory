"use strict";
exports.__esModule = true;
var react_1 = require("react");
var mdx_macro_1 = require("mdx.macro");
var Jumbotron_1 = require("react-bootstrap/Jumbotron");
function Content() {
    var MDX = react_1.lazy(function () { return mdx_macro_1.importMDX('./Content.mdx'); });
    return (react_1["default"].createElement(react_1.Fragment, null,
        react_1["default"].createElement(react_1.Suspense, { fallback: react_1["default"].createElement("div", null, " Loading...") },
            react_1["default"].createElement(Jumbotron_1["default"], { "data-tut": "reactour__sujet" },
                react_1["default"].createElement(MDX, null)))));
}
exports["default"] = Content;
