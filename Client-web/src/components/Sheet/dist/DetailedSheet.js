"use strict";
exports.__esModule = true;
exports.ObjectSheet = exports.ProfileSheet = void 0;
var react_1 = require("react");
var react_bootstrap_1 = require("react-bootstrap");
var data_1 = require("../../services/data");
var ObjectSheet = function (props) {
    return (react_1["default"].createElement("aside", null,
        react_1["default"].createElement(react_bootstrap_1.Table, { className: "bg-white text-black", size: "sm", bordered: true, hover: true, variant: "" },
            react_1["default"].createElement("thead", null,
                react_1["default"].createElement("tr", null,
                    react_1["default"].createElement("th", null, "#"),
                    react_1["default"].createElement("th", null, "Data"))),
            react_1["default"].createElement("tbody", null, props.info ? Object.keys(props.info).map(function (key, i) {
                return (react_1["default"].createElement("tr", { key: i },
                    react_1["default"].createElement("td", null, key),
                    react_1["default"].createElement("td", null, props.info[key])));
            }) : ""))));
};
exports.ObjectSheet = ObjectSheet;
var ProfileSheet = function () {
    var _a = react_1.useState({}), result = _a[0], setResult = _a[1];
    react_1.useEffect(function () {
        data_1.search("lu").then(function (data) { return setResult(data); });
    }, []);
    return (react_1["default"].createElement(react_bootstrap_1.Table, { striped: true, bordered: true, hover: true, variant: "dark" },
        react_1["default"].createElement("thead", null,
            react_1["default"].createElement("tr", null,
                react_1["default"].createElement("th", null, "#"),
                react_1["default"].createElement("th", null, "First Name"),
                react_1["default"].createElement("th", null, "Last Name"),
                react_1["default"].createElement("th", null, "Username"))),
        react_1["default"].createElement("tbody", null,
            react_1["default"].createElement("tr", null,
                react_1["default"].createElement("td", null, "1"),
                react_1["default"].createElement("td", null, "Mark"),
                react_1["default"].createElement("td", null, "Otto"),
                react_1["default"].createElement("td", null, "@mdo")),
            react_1["default"].createElement("tr", null,
                react_1["default"].createElement("td", null, "2"),
                react_1["default"].createElement("td", null, "Jacob"),
                react_1["default"].createElement("td", null, "Thornton"),
                react_1["default"].createElement("td", null, "@fat")),
            react_1["default"].createElement("tr", null,
                react_1["default"].createElement("td", null, "3"),
                react_1["default"].createElement("td", null, "Larry the Bird"),
                react_1["default"].createElement("td", null, "@twitter")))));
};
exports.ProfileSheet = ProfileSheet;
