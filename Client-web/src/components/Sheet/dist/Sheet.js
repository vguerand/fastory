"use strict";
exports.__esModule = true;
var react_1 = require("react");
var lodash_1 = require("lodash");
var react_bootstrap_1 = require("react-bootstrap");
var data_1 = require("../../services/data");
var DetailedSheet_1 = require("./DetailedSheet");
var react_router_1 = require("react-router");
var moment_1 = require("moment");
var Sheet = function () {
    var _a = react_1.useState([]), result = _a[0], setResult = _a[1];
    var location = react_router_1.useLocation();
    var query = location.search.substring(8);
    react_1.useEffect(function () {
        if (query) {
            data_1.search(query).then(function (data) {
                setResult(lodash_1["default"].concat(result, data[0]));
                console.log(data[0], "data");
            })["catch"](function () { return setResult(null); });
        }
    }, []);
    if (result != null) {
        console.log(JSON.stringify(result), " ???");
        return (react_1["default"].createElement(react_1["default"].Fragment, null, result.map(function (el, i) {
            if (el) {
                return (react_1["default"].createElement(react_1["default"].Fragment, null,
                    react_1["default"].createElement(react_bootstrap_1.Card, { key: i, className: "bg-dark text-white" },
                        react_1["default"].createElement(react_bootstrap_1.Card.Img, { src: "fond_ecran3.jpg", alt: "Card image" }),
                        react_1["default"].createElement(react_bootstrap_1.Card.ImgOverlay, null,
                            react_1["default"].createElement(react_bootstrap_1.Card.Title, null,
                                el.name || el.title,
                                " "),
                            react_1["default"].createElement(react_bootstrap_1.Card.Text, null, el.classification || el.producer),
                            react_1["default"].createElement(react_bootstrap_1.Card.Text, null, moment_1["default"](el.created).format('MMM DD YYYY h:mm A')),
                            react_1["default"].createElement(DetailedSheet_1.ObjectSheet, { info: el }))),
                    react_1["default"].createElement("br", null),
                    react_1["default"].createElement("hr", null)));
            }
            else
                return react_1["default"].createElement("h1", { key: i }, " No Results");
        })));
    }
    return react_1["default"].createElement("h1", null, " No Results");
};
exports["default"] = Sheet;
