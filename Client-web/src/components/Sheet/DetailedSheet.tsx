import React, { useState, useEffect } from 'react'
import _ from "lodash";
import { Table } from 'react-bootstrap';
import { search } from "../../services/data"

interface ObjectSheetProps {
    info: any;
}

const ObjectSheet = (props: ObjectSheetProps) => {
    return (
        <aside>
            <Table  className="bg-white text-black"  size="sm"  bordered hover variant="">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <tbody>
                    {props.info ?  Object.keys(props.info).map((key, i) => {
                        return (
                          <tr key={i}>
                            <td>{key}</td>
                            <td>{props.info[key]}</td>
                        </tr>);
                    }): ""}
                </tbody>
            </Table>
        </aside>
    )
}


const ProfileSheet = () => {
    const [result, setResult] = useState<any>({});
    useEffect(() => {
        search("lu").then(data => setResult(data))
    }, [])

    return (
        <Table striped bordered hover variant="dark">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Larry the Bird</td>
                    <td>@twitter</td>
                </tr>
            </tbody>
        </Table>

    )
}


export { ProfileSheet, ObjectSheet };