import React, { useState, useEffect } from 'react'
import _ from "lodash";
import { Card } from 'react-bootstrap';
import { search } from "../../services/data"
import { ObjectSheet } from './DetailedSheet';
import { useLocation } from 'react-router';
import moment from "moment";


const Sheet = () => {
    const [result, setResult]: any = useState([]);
    const location = useLocation();
    const query = location.search.substring(8);
    useEffect(() => {
        if (query) {
            search(query).then(data => {
                setResult(_.concat(result, data[0]))
                console.log(data[0], "data")
            }).catch(() => setResult(null))
        }
    }, [])

    if (result != null) {
        console.log(JSON.stringify(result), " ???")
        return (
            <>
                {result.map((el, i) => {
                    if (el) {
                    return (
                        <>

                        <Card key={i} className="bg-dark text-white">
                            <Card.Img src="fond_ecran3.jpg" alt="Card image" />
                            <Card.ImgOverlay>
                                <Card.Title>{el.name || el.title} </Card.Title>
                                <Card.Text>
                                    {el.classification || el.producer}
                                </Card.Text>
                                <Card.Text>{moment(el.created).format('MMM DD YYYY h:mm A')}</Card.Text>
                                <ObjectSheet info={el} />
                            </Card.ImgOverlay>
                        </Card>
                        <br/>
                        <hr/>
                        </>
                    )} else return <h1 key={i}> No Results</h1>;
                })}
            </>
        )
    }
    return <h1> No Results</h1>
}


export default Sheet;