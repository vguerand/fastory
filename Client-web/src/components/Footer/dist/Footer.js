"use strict";
exports.__esModule = true;
var react_1 = require("react");
function Footer() {
    return (react_1["default"].createElement("footer", { className: "footer" },
        react_1["default"].createElement("div", { className: "content has-text-centered" },
            react_1["default"].createElement("p", null,
                react_1["default"].createElement("strong", null, " SWAPI test"),
                react_1["default"].createElement("a", { href: "https://www.fastory.io/fr/" }, " Fastory"),
                "."))));
}
exports["default"] = Footer;
