import React from "react";
import { Switch, Route } from "react-router";
import {
  HomePage,
  ResultPage,
  TimelinePage
} from "./views";

function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route exact path="/result" component={ResultPage} />
      <Route exact path="/planets" component={TimelinePage} />
      <Route exact path="/people" component={TimelinePage} />
      <Route exact path="/films" component={TimelinePage} />
      <Route exact path="/species" component={TimelinePage} />
      <Route exact path="/vehicles" component={TimelinePage} />
      <Route exact path="/starships" component={TimelinePage} />
      <Route exact path="*" component={HomePage} />
    </Switch>
  );
}
export default Routes;
