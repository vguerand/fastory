"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_redux_1 = require("react-redux");
var Routes_1 = require("./Routes");
var Navbar_1 = require("./components/Navbar/Navbar");
var connexion_1 = require("./services/connexion");
var react_device_detect_1 = require("react-device-detect");
function App() {
    return (react_1["default"].createElement("div", { className: "bg-light", style: {
            backgroundImage: "url(stars.jpg)"
        } },
        react_1["default"].createElement(connexion_1["default"], null,
            react_1["default"].createElement(react_device_detect_1.BrowserView, null,
                react_1["default"].createElement(Navbar_1["default"], null),
                react_1["default"].createElement(Routes_1["default"], null)),
            react_1["default"].createElement(react_device_detect_1.MobileView, null,
                react_1["default"].createElement(Navbar_1["default"], null),
                react_1["default"].createElement(Routes_1["default"], null)))));
}
exports["default"] = react_redux_1.connect(function (store) { return store; })(App);
