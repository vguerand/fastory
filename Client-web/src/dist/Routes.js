"use strict";
exports.__esModule = true;
var react_1 = require("react");
var react_router_1 = require("react-router");
var views_1 = require("./views");
function Routes() {
    return (react_1["default"].createElement(react_router_1.Switch, null,
        react_1["default"].createElement(react_router_1.Route, { exact: true, path: "/", component: views_1.HomePage }),
        react_1["default"].createElement(react_router_1.Route, { exact: true, path: "/result", component: views_1.ResultPage }),
        react_1["default"].createElement(react_router_1.Route, { exact: true, path: "/planets", component: views_1.TimelinePage }),
        react_1["default"].createElement(react_router_1.Route, { exact: true, path: "/people", component: views_1.TimelinePage }),
        react_1["default"].createElement(react_router_1.Route, { exact: true, path: "/films", component: views_1.TimelinePage }),
        react_1["default"].createElement(react_router_1.Route, { exact: true, path: "/species", component: views_1.TimelinePage }),
        react_1["default"].createElement(react_router_1.Route, { exact: true, path: "/vehicles", component: views_1.TimelinePage }),
        react_1["default"].createElement(react_router_1.Route, { exact: true, path: "/starships", component: views_1.TimelinePage }),
        react_1["default"].createElement(react_router_1.Route, { exact: true, path: "*", component: views_1.HomePage })));
}
exports["default"] = Routes;
