/**
 * To Complete
 */
interface UserInterface {
    id?: string;
    email?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    createdAt?: Date;
    bio?: string; // presentation
  }
  export default UserInterface;
  