"use strict";
exports.__esModule = true;
var react_1 = require("react");
var Sheet_1 = require("../components/Sheet/Sheet");
var react_bootstrap_1 = require("react-bootstrap");
function ResultPage() {
    return (react_1["default"].createElement(react_bootstrap_1.Container, null,
        react_1["default"].createElement(react_bootstrap_1.Jumbotron, null,
            react_1["default"].createElement("h1", null, " Results of the mysterious request")),
        react_1["default"].createElement(Sheet_1["default"], null)));
}
exports["default"] = ResultPage;
