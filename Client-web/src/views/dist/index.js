"use strict";
exports.__esModule = true;
exports.ResultPage = exports.TimelinePage = exports.HomePage = void 0;
var HomePage_1 = require("./HomePage");
exports.HomePage = HomePage_1["default"];
var TimelinePage_1 = require("./TimelinePage");
exports.TimelinePage = TimelinePage_1["default"];
var ResultPage_1 = require("./ResultPage");
exports.ResultPage = ResultPage_1["default"];
