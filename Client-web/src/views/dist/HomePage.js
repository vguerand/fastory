"use strict";
exports.__esModule = true;
var react_1 = require("react");
var Container_1 = require("react-bootstrap/Container");
var Content_1 = require("./../components/Mdx/Content");
var reactour_1 = require("reactour");
var TourConfig_1 = require("../components/Tour/TourConfig");
function HomePage() {
    var _a = react_1.useState(true), isTourOpen = _a[0], setIsTourOpen = _a[1];
    return (react_1["default"].createElement(Container_1["default"], null,
        react_1["default"].createElement(Content_1["default"], null),
        react_1["default"].createElement(reactour_1["default"], { steps: TourConfig_1["default"], isOpen: isTourOpen, onRequestClose: function () { return setIsTourOpen(false); } })));
}
exports["default"] = HomePage;
