"use strict";
exports.__esModule = true;
var react_1 = require("react");
var Jumbotron_1 = require("react-bootstrap/Jumbotron");
var Container_1 = require("react-bootstrap/Container");
var Button_1 = require("react-bootstrap/Button");
var Timeline_1 = require("../components/Timeline/Timeline");
var data_1 = require("../services/data");
var react_router_dom_1 = require("react-router-dom");
var lodash_1 = require("lodash");
function TimelinePage() {
    var _a = react_1.useState(true), isTourOpen = _a[0], setIsTourOpen = _a[1];
    var _b = react_1.useState(0), page = _b[0], setPage = _b[1];
    var location = react_router_dom_1.useLocation();
    var _c = react_1.useState([]), result = _c[0], setResult = _c[1];
    var _d = react_1.useState(0), count = _d[0], setCount = _d[1];
    react_1.useEffect(function () {
        data_1.resultCategorie(location.pathname, page).then(function (data) {
            setCount(data.count);
            setResult(lodash_1["default"].concat(result, data.results));
        })["catch"](function () { return console.log("no more data"); });
    }, [page]);
    if (!lodash_1["default"].isEmpty(result)) {
        return (react_1["default"].createElement(react_1["default"].Fragment, null,
            react_1["default"].createElement(Jumbotron_1["default"], null,
                react_1["default"].createElement("h1", null,
                    count,
                    " results for ",
                    location.pathname)),
            react_1["default"].createElement(Timeline_1["default"], { listTime: result }),
            react_1["default"].createElement(Container_1["default"], null,
                react_1["default"].createElement(Button_1["default"], { onClick: function () { return setPage(page + 1); } }, " Load More"))));
    }
    return react_1["default"].createElement("div", null, "  Loading .. ");
}
exports["default"] = TimelinePage;
