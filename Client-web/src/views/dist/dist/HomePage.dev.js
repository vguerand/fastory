"use strict";

exports.__esModule = true;

var react_1 = require("react");

var Jumbotron_1 = require("react-bootstrap/Jumbotron");

var Toast_1 = require("react-bootstrap/Toast");

var Container_1 = require("react-bootstrap/Container");

var Button_1 = require("react-bootstrap/Button");

var reactour_1 = require("reactour");

var steps = [{
  selector: '.first-step',
  content: 'This is my first Step'
}];

var ExampleToast = function ExampleToast(_a) {
  var children = _a.children;

  var _b = react_1.useState(true),
      show = _b[0],
      toggleShow = _b[1];

  return react_1["default"].createElement(react_1["default"].Fragment, null, !show && react_1["default"].createElement(Button_1["default"], {
    onClick: function onClick() {
      return toggleShow(true);
    }
  }, "Show Toast"), react_1["default"].createElement(Toast_1["default"], {
    show: show,
    onClose: function onClose() {
      return toggleShow(false);
    }
  }, react_1["default"].createElement(Toast_1["default"].Header, null, react_1["default"].createElement("strong", {
    className: "mr-auto"
  }, "React-Bootstrap")), react_1["default"].createElement(Toast_1["default"].Body, null, children)));
};

function HomePage() {
  var _a = react_1.useState(false),
      isTourOpen = _a[0],
      setIsTourOpen = _a[1];

  var name = "name";
  var description = "description";
  return react_1["default"].createElement(Container_1["default"], null, react_1["default"].createElement(ExampleToast, {
    children: "YAAA"
  }), react_1["default"].createElement(Jumbotron_1["default"], null, react_1["default"].createElement("h1", null, name), react_1["default"].createElement("p", null, description), react_1["default"].createElement("p", {
    className: "d-flex "
  }, "Languages Wookiee"), react_1["default"].createElement("p", null, react_1["default"].createElement(Button_1["default"], {
    variant: "primary"
  }, "Learn more"))), react_1["default"].createElement(reactour_1["default"], {
    steps: steps,
    isOpen: isTourOpen,
    onRequestClose: function onRequestClose() {
      return setIsTourOpen(false);
    }
  }));
}

exports["default"] = HomePage;