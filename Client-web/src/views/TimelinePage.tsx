import React, { useState, useEffect } from 'react'
import { importMDX } from 'mdx.macro'
import Jumbotron from 'react-bootstrap/Jumbotron';
import Toast from 'react-bootstrap/Toast';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Timeline from "../components/Timeline/Timeline";
import { resultCategorie } from "../services/data"
import { useLocation } from 'react-router-dom'
import _ from "lodash";

function TimelinePage() {
  const [isTourOpen, setIsTourOpen] = useState(true);
  const [page, setPage] = useState(0);
  const location = useLocation();
  const [result, setResult]: any = useState([]);
  const [count, setCount] = useState(0);

  useEffect(() => {
    resultCategorie(location.pathname, page).then(data => 
      {
        setCount(data.count)
        setResult(_.concat(result, data.results))
      }).catch(() => console.log("no more data"))
  }, [page])

  if (!_.isEmpty(result)) {
    return (
      <>
        <Jumbotron>
          {/* {JSON.stringify(result)} */}
          <h1>{count} results for {location.pathname}</h1>
        </Jumbotron>
        <Timeline listTime={result} />

        <Container>
          <Button onClick={() => setPage(page + 1)}> Load More</Button>
        </Container>

      </>
    );
  }
  return <div>  Loading .. </div>
}


export default TimelinePage;
