import React, { useState } from 'react'
import Sheet from '../components/Sheet/Sheet';
import { Jumbotron , Container} from 'react-bootstrap';

function ResultPage() {
  return (
    <Container>
        <Jumbotron>
          {/* {JSON.stringify(result)} */}
          <h1> Results of the mysterious request</h1>
        </Jumbotron>
        <Sheet />
    </Container>
  );
}


export default ResultPage;
