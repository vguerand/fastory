import React, { useState } from 'react'
import Container from 'react-bootstrap/Container';
import Content from "./../components/Mdx/Content"
import Tour from 'reactour';
import TourConfig from "../components/Tour/TourConfig"

function HomePage() {
  const [isTourOpen, setIsTourOpen] = useState(true);
  return (
    <Container>
      {/* I don't have the time to make the option :(  */}
      {/* <Jumbotron>
        <h1>Swapi</h1>
        <p><a href="https://www.fastory.io/fr/exemples">Fastory</a>  technical test </p>
        <Form data-tut="reactour__search_filters">
          <Row>
            <Col>
              <Form.Control type="text" placeholder="Search" />
            </Col>
            <Col>
              <Form.Check type="checkbox" id={`check-api-checkbox`}>
                <Form.Check.Input type="checkbox" />
                <Form.Check.Label>Languages Wookiee</Form.Check.Label>
              </Form.Check>
              <Form.Check type="checkbox" id={`check-api-checkbox`}>
                <Form.Check.Input type="checkbox" />
                <Form.Check.Label>Timeline view</Form.Check.Label>
              </Form.Check>
            </Col>
            <Col>
              <Button variant="primary">Explore</Button>
            </Col>
          </Row>
        </Form>
      </Jumbotron> */}
      <Content />
      <Tour
        steps={TourConfig}
        isOpen={isTourOpen}
        onRequestClose={() => setIsTourOpen(false)} />
    </Container>
  );
}


export default HomePage;
