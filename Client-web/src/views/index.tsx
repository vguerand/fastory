import HomePage from "./HomePage";
import TimelinePage from "./TimelinePage";
import ResultPage from "./ResultPage";



export {
  HomePage,
  TimelinePage,
  ResultPage
};
