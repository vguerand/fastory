

const location = () => Object.fromEntries(new URLSearchParams(window.location.search));

export { location }