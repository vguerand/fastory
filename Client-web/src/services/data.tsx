import React, { useState } from "react";
import axios from "axios";



const search = async (search) => {

    const result = Promise.resolve(axios
        .get("http://localhost:3030/?search=" + search)
        .then((reponse) => reponse.data)
        .catch((erreur) => console.error(erreur)));
    console.log(JSON.stringify(result));
    return result;
}

const searchForTimeLine = async (search) => {
    const result = Promise.resolve(axios
        .get("http://localhost:3030/?search=" + search)
        .then((reponse) => reponse.data)
        .catch((erreur) => console.error(erreur)));
    console.log(result);
    return result;
}


const resultCategorie = async (categorie, page) => {
    if (page > 0) {
        return Promise.resolve(axios
            .get("http://localhost:3030" + categorie + "?page=" + page)
            .then((reponse) => reponse.data)
            .catch((erreur) => console.error(erreur)));
    }
    return Promise.resolve(axios
        .get("http://localhost:3030" + categorie)
        .then((reponse) => reponse.data)
        .catch((erreur) => console.error(erreur)));
}

export {
    search,
    searchForTimeLine,
    resultCategorie
}