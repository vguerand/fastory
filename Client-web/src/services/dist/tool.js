"use strict";
exports.__esModule = true;
exports.location = void 0;
var location = function () { return Object.fromEntries(new URLSearchParams(window.location.search)); };
exports.location = location;
